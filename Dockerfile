FROM postgres:9.6.8

LABEL maintainer="anders.harrisson@esss.se"

COPY postgresql.conf /usr/share/postgresql/postgresql.conf.sample
COPY pg_hba.conf /usr/share/postgresql/9.6/pg_hba.conf.sample
